const assert = require('assert')
const _ = require('lodash')

describe('NodeModules.class : ', function() {

  it('can be imported', function() {
    require('..')
  })

  describe('In Action : ', function() {
    const NodeModules = require('..')

    describe('Configurable Location : ', function(){

      it('can detect the current folder as the default.', function() {
        let m = new NodeModules()

        assert.notEqual( typeof(m.root) , 'undefined' )
      })

      it('will accept an overridden path', function() {
        const opts = {root:'test/path'}
        let m = new NodeModules(opts)

        assert.equal(m.root, opts.root)
      })
    })

    describe('Child Process :', function() {
      it('`npm list --json` as `.json`', function() {
        let m = new NodeModules()

        const output = m.json

        assert(output.length > 100, '`.json` output be of a decent size.')
        assert(output.endsWith('}'), '`.json` output should end with a `}`')

        //parse and validate
        const parsed = JSON.parse(output)
        assert(!!parsed.name, 'Parsed output should have valid name')
        assert(!!parsed.version, 'Parsed output should have valid version')
        assert(!!parsed.dependencies, 'Parsed output should have valid dependencies.')
        assert(!!parsed.dependencies.lodash, 'Should at least find lodash in this module.')
      }).timeout(4000)

      it('`npm list --parseable` as `.parseable`', function() {
        let m = new NodeModules()

        const output = m.parseable

        assert(output.length > 20, 'Output should be at least two paths worth.')
      }).timeout(4000)

    })

    describe('Post Processing :', function(){

      it('splits `.parseable` into `.path` array', function() {
        let m = new NodeModules()
        const array = m.paths

        assert.equal(typeof(array), typeof([]), 'paths should be an array.')

        assert(array.length > 1, 'should have at least the main module + lodash.')
      }).timeout(4000)

      it('merges `.json` and `.parseable` into `.processed`', function() {
        let m = new NodeModules()

        const processed = m.processed

        assert(!!processed.path, 'main module should have resolved path.')

        for (const d in processed.dependencies)
        {
          const dep = processed.dependencies[d]
          assert(!!dep.name, 'Every dependency should have `.name` added to its descriptor.')
          assert(!!dep.path, 'Every dependency should have `.path` added to its descriptor.')
        }

        function IterableCheck(root)
        {
          // Each node should be iterable.
          assert(!!root[Symbol.iterator], `Descriptor for '${root.name}' should be iterable`)
          for (const mod of root)
          {
            assert(!!mod.name, 'Iterator elements should have `.name`')
            assert(!!mod.path, 'Iterator elements should have `.path`')
          }

          // dependencies object and members should be iterable.
          if (!!root.dependencies)
          {
            assert(!!root.dependencies[Symbol.iterator], `${root.name}.dependencies should be iterable.`)
            for (let mod of root.dependencies)
            {
              IterableCheck(mod)
            }
          }
        }

        IterableCheck(processed)
      }).timeout(10000)

      it ('flattens `.processed` into `.flattened`', function() {
        let m = new NodeModules()
        const flat = m.flattened

        for (const dep of flat)
        {
          const match = _.find(flat, (_dep) => _dep.name == dep.name)
          assert(!!match, `All modules should have their dependencies in the flat list: ${dep.name}`)
        }
      }).timeout(6000)
    })
  })
})
