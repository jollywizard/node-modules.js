# node-modules.js

## Overview

`node-modules.js` is a node helper for using `npm list` to produce and process dependency information about a module.

## Installation

From the repo:

```
npm install git://git@bitbucket.org:jollywizard/node-modules.js.git
```

## Usage

### Importing the module

```
const NodeModules = require('@jamesarlow/node-modules')
```

### Instantiation

The module is provided as a class, create it with the `new` keyword.

In the parameter object, use `root` to specify the module root directory.

```
let modules = new NodeModules({root:'path/to/module/root'})
```

Or let the current working directory be the default, i.e. `{root:process.cwd()}`.

```
let modules = new NodeModules()
```

### Data Properties

Once the module has been created, you can use the data properties to work with the `npm list` data.

#### Direct NPM Methods

These values call NPM directly.  The string output from the terminal is returned directly.

* `modules.json` : `npm list --json`

* `modules.parseable` : `npm list --parseable`

#### Post Processing Methods

These methods apply processing to the Direct NPM Methods, to make them more useful.

* `modules.paths` : Splits `modules.parseable` into an array of string paths.

* `modules.processed` : Merges `.json` and `.parseable` into a tree Object which details the dependency hierarchy.

    * Dependency module names are copied into their descriptors so they have the same format as the root node, and can be browsed individually.

    * All nodes have their path added from `.paths`.

    * All nodes and `node.dependencies` become iterables.  Iterators produce the current node and all dependency children (recursively).

* `modules.flatten` : Flattens module.processed into a 1 dimensional array.  

    * Equivalent to `for (let mod of modules.processed)`
