const child = require('child_process')
const fs = require('fs')
const _ = require('lodash')

/**
  The `Modules` class provides a wrapper for a directory path which can be used
  to access raw `npm list` output as well as processed trees and arrays based on
  that output.

  ## Call `npm list`

  ```
  const modules = new NodeModules()

  // Calls `npm list --json`
  modules.json

  // Calls `npm list --parseable`
  modules.parseable
  ```

  ## Post Processing

  ```
  const modules = new NodeModules()

  // Tree based on: `.json` + `.parseable`
  modules.processed

  // Array traversal of: `.processed`
  modules.flatten
  ```
*/
class NodeModules {

  /**
    @param {Object} opts {
      `root`  : The path to the module root (i.e.`node_modules` parent).
    , `cache` : If true, the value will cache the results of each data property.
    }
  */
  constructor(opts)
  {
    if (!!opts)
    {
      this.cache = (!!opts.cache)
      this.root = opts.root
    }
    if (!('cache' in this))
      this.cache = true

    if (!this.root)
      this.root = process.cwd()
  }

  /**
    @return {String} The nested installation path for locally installed modules.
  */
  get path() {
    return `${this.root}/node_modules`
  }

  /**
    @return {boolean} If `this.root` exists as a directory.
  */
  get exists() {
    return fs.existsSync(this.path) && fs.stat(this.path).isDirectory()
  }

  /**
    Parses the output of `.json` into an object, then ensures every descriptor
    in the tree contains the module specific `.name` and `.path`.

    @return {Object} A processed merger of `.json` and `.paths`
  */
  get processed() {
    if (!this.cached || !this._processed)
    {
      const json = this.json
      const paths = this.paths

      // convert to object
      const parsed = JSON.parse(json)

      // Top level value has name, but needs path.
      parsed.path = _.first(paths)
      parsed[Symbol.iterator] = FlatModuleTreeIterator(parsed)

      // Recursively copy path and name to all nested values.
      function ProcessDependencies(root) {
        const dependencies = root.dependencies

        // The dependency object is also iterable.
        if (!!dependencies)
          dependencies[Symbol.iterator] = FlatDependenciesIterator(dependencies)

        for (const key in dependencies)
        {
          const dep = dependencies[key]
          dep.name = key
          dep.path = _.find(_.tail(paths), (path) => path.includes(dep.name))
          dep[Symbol.iterator] = FlatModuleTreeIterator(dep)
          ProcessDependencies(dep)
        }
      }

      ProcessDependencies(parsed)

      this._processed = parsed
    }

    return this._processed
  }

  /**
    @return {String} The output of `npm list --json`.
  */
  get json() {
    if (!this.cached || !this._json)
    {
      const npm_ls_json = child.spawnSync('npm', ['ls', '--json'], {cwd:this.root})
      this._json = npm_ls_json.output.join('')
      this._json = this._json.substring(0, 1 + this._json.lastIndexOf('}'))
    }
    return this._json
  }


  /**
    @return {String} The output of `npm list --parseable`.
  */
  get parseable() {
    if (!this.cached || !this._parseable)
    {
      const npm_ls_parseable = child.spawnSync('npm', ['ls', '--parseable'], {cwd:this.root})
      this._parseable = npm_ls_parseable.output.join('')
    }
    return this._parseable
  }

  /**
    @return {Array<String>} The raw output of `.parseable`, but split into an array.
  */
  get paths() {
    return this.parseable.split(/\s+/).filter( str => str.length > 0 )
  }

  /**
    @return {Array} A flattened version of the tree from `.processed`.
  */
  get flattened() {
    return FlattenModuleTree(this.processed)
  }

  [Symbol.iterator] () {
    return this.flattened[Symbol.iterator]()
  }
}

function FlattenModuleTree(root, accumulator = [])
{
  if (!root) return

  accumulator.push(root)

  FlattenDependencies(root.depen)
  if (!!root.dependencies)
  {
    for (const dep in root.dependencies)
    {
      FlattenModuleTree(root.dependencies[dep], accumulator)
    }
  }

  return accumulator
}

function FlattenDependencies(dependencies, accumulator = [])
{
  if (!dependencies) return

  for (const key in dependencies)
  {
    const dep = dependencies[key]
    FlattenModuleTree(dep, accumulator)
  }

  return accumulator
}

function FlatModuleTreeIterator(root)
{
  return () => FlattenModuleTree(root)[Symbol.iterator]()
}

function FlatDependenciesIterator(dependencies)
{
  return () => FlattenDependencies(dependencies)[Symbol.iterator]()
}

module.exports = NodeModules
